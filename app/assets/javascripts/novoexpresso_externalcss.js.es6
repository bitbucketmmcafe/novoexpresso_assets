$(document).ready(function() {
  setListenMessage();
});

const setListenMessage = () => {
   if (window.addEventListener) {
     window.addEventListener("message", listenMessage, false);
   } else {
     window.attachEvent("onmessage", listenMessage);
   }
};

const listenMessage = (event) => {
  if(event.data.type == "changecolor") {
    window.localStorage.setItem("parentcss", event.data.csspath)
    let cssexternalcolortag = document.getElementById("cssexternalcolor");
    cssexternalcolortag.setAttribute("href", event.data.csspath);
    $.ajax({
      url: $('body').data('context') + 'session_data.json',
      data: {
        key: "externalcss_color_path",
        value: event.data.csspath
      },
      method: "post"
    });
  };
  if(event.data.type == "setmaincss") {
    let stylecss = document.getElementById("stylecss");
    stylecss.setAttribute("href", event.data.cssgeral);
    $.ajax({
      url: $('body').data('context') + 'session_data.json',
      data: {
        key: "externalcss_path",
        value: event.data.cssgeral
      },
      method: "post"
    });
  };

};
