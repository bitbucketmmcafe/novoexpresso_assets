"use strict";

$(document).ready(function () {
  setListenMessage();
});

var setListenMessage = function setListenMessage() {
  if (window.addEventListener) {
    window.addEventListener("message", listenMessage, false);
  } else {
    window.attachEvent("onmessage", listenMessage);
  }
};

var listenMessage = function listenMessage(event) {
  if (event.data.type == "changecolor") {
    window.localStorage.setItem("parentcss", event.data.csspath);
    var cssexternalcolortag = document.getElementById("cssexternalcolor");
    cssexternalcolortag.setAttribute("href", event.data.csspath);
    $.ajax({
      url: $('body').data('context') + 'session_data.json',
      data: {
        key: "externalcss_color_path",
        value: event.data.csspath
      },
      method: "post"
    });
  };
  if (event.data.type == "setmaincss") {
    var stylecss = document.getElementById("stylecss");
    stylecss.setAttribute("href", event.data.cssgeral);
    $.ajax({
      url: $('body').data('context') + 'session_data.json',
      data: {
        key: "externalcss_path",
        value: event.data.cssgeral
      },
      method: "post"
    });
  };
};