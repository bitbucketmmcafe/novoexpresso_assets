$(document).on('turbolinks:load', () => {
	let parentcss = document.getElementById("stylecss");

	if(!parentcss || parentcss.getAttribute("href")=="")
		return false;
	MMLoaderFunctions.success();
});



const MMLoaderFunctions = {

	error: () => {
		if ($(".mmloader-item-error")) {
			$(".mmloader-item-error").addClass("alert-dismiss_notice").removeClass("alert-error");
			setTimeout(() => {
				$(".mmloader-item-error").fadeOut();
			}, 2000);
		}
	},
	success: () => {
		if($(".mmloader-item-success")) {
			$(".mmloader-item-success").addClass("alert-dismiss_notice").removeClass("alert-success");
			setTimeout( () => {
			$(".alert-dismiss_notice").fadeOut();
			}, 2000);  
		}
	},
	build_api_errors: json => {
		let errors = [];
		for(let key in json) {
			json[key].forEach( error => {
				errors.push(I18n.t(`${key}s.singular`) + " " + error);	
			});
		}
		return `<div class="alert alert-danger mm-alert-error mmloader-item-error">
						<div class="col-sm-12">
						<p>
						${errors.join("<br />")}
						</p>
						</div>
						</div>
					`;
	},
	build_api_success: msg => {
		return `<div class="alert alert-success mmloader-item-success">
				${msg}
				</div>`;
	},
	build_plain_error: error_msg => {
		return `<div class="alert alert-danger mm-alert-error mmloader-item-error">
						<div class="col-sm-12">
						<p>
						${error_msg}
						</p>
						</div>
						</div>
					`;
	}
}