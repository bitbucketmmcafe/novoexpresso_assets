
$(document).on('turbolinks:load', function () {
	var parentcss = document.getElementById("stylecss");

	if (!parentcss || parentcss.getAttribute("href") == "") return false;
	MMLoaderFunctions.success();
});

var MMLoaderFunctions = {

	error: function error() {
		if ($(".mmloader-item-error")) {
			$(".mmloader-item-error").addClass("alert-dismiss_notice").removeClass("alert-error");
			setTimeout(function () {
				$(".mmloader-item-error").fadeOut();
			}, 2000);
		}
	},
	success: function success() {
		if ($(".mmloader-item-success")) {
			$(".mmloader-item-success").addClass("alert-dismiss_notice").removeClass("alert-success");
			setTimeout(function () {
				$(".alert-dismiss_notice").fadeOut();
			}, 2000);
		}
	},
	build_api_errors: function build_api_errors(json) {
		var errors = [];

		var _loop = function (key) {
			json[key].forEach(function (error) {
				errors.push(I18n.t(key + ".singular") + " " + error);
			});
		};

		for (var key in json) {
			_loop(key);
		}
		return "<div class=\"alert alert-danger mm-alert-error mmloader-item-error\"><div class=\"col-sm-12\"><p>" + errors.join("<br />") + "</p></div></div>";
	},
	build_api_success: function build_api_success(msg) {
		return "<div class=\"alert alert-success mmloader-item-success\">" + msg + "</div>";
	},
	build_plain_error: function(error_msg) {
		return '<div class="alert alert-danger mm-alert-error mmloader-item-error">' +
				'<div class="col-sm-12">' +
					'<p>' + error_msg + '</p>' +
				'</div>' +
				'</div>';
	}
};