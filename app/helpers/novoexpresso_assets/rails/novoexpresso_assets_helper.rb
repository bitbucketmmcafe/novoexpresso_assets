module NovoexpressoAssets
	module Rails
		module NovoexpressoAssetsHelper

			def novoexpresso_css_tags
				"<link media='screen' id='stylecss' rel='stylesheet' type='text/css' href='#{session[:externalcss_path]}'>
				<link media='screen' id='cssexternalcolor' rel='stylesheet' type='text/css' href='#{session[:externalcss_color_path]}'>".html_safe
			end

		end
	end
end