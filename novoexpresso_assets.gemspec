# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'novoexpresso_assets'
  spec.version       = '0.0.4.7'
  spec.authors       = ['Christian Benseler']
  spec.email         = ['cbenseler@mmcafe.com.br']
  spec.description   = %q{Integratation with Novo Expresso }
  spec.summary       = %q{Integrates a module, as frame, with Novo Expresso}
  spec.homepage      = 'https://mmcafe.com.br'
  spec.license       = 'MIT'

  #spec.files         = `git ls-files`.split($/)
  #spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  #spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  #spec.require_paths = ['lib']

  spec.files       = `git ls-files`.split("\n")

  spec.add_development_dependency 'sass-rails'
  spec.add_development_dependency 'sass'
  spec.add_development_dependency 'sprockets', '3.5.2'
  spec.add_development_dependency 'sprockets-es6'
end